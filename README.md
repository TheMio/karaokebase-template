# Karaoke Mugen's Karaoke database

## Format

A karaoke is made of the following elements :

* A `.kara.json` file in the `karaokes` folder
* A `.ass` file in the `lyrics` folder
* A video or audio file in the `medias` folder
* One or more `.tag.json` files in the `tags` folder

Optionally `.hook.yml` files in the `hooks` folder for tag automation when creating/editing songs in the app

### `karaokes` folder

This folder holds files with karaoke metadata such as video file, lyrics file, etc. It uses the standard JSON format.

### `lyrics` folder

This folder contains the subtitles files as specified in the `.kara.json` file.

It is generally a `.ass` file made with Aegisub. See [the contribution guide](CONTRIBUTING.md) for a more detailed tutorial on how to write good karaokes.

### `medias` folder

This folder contains the video or audio file as specified in the `.kara.json` file.

Videos aren't included in this git repository or else it'd be way too huge (about several hundred gigabytes at the moment). KM will take care of these for you.

**If you get errors from Karaoke Mugen during database generation / validation, make sure you have the latest version of the repository (`git pull` or downloaded the latest `master.zip`).**

### `tags` folder

A song has many tags defining its metadata, like singers, songwriters, creators, genres, etc. These are used by the search engine.
